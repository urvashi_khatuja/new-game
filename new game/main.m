//
//  main.m
//  new game
//
//  Created by urvashi khatuja on 5/27/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
