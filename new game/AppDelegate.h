//
//  AppDelegate.h
//  new game
//
//  Created by urvashi khatuja on 5/27/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

